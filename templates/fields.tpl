<script type="text/javascript">
      window.contrib_id = {$contrib_id};
</script>
{literal}
<script type="text/javascript">

      cj(function(){
        function get_field_label(id){
          return CRM.api3('CustomField', 'get', {
              "sequential": 1,
              "id": id
          }).done(function(result) {
              window.field_labels[id] = result.values[0].label;
          }); 
        }

        if (cj('questions').length ==0 ) return;
        cj('questions').text('One moment...');

        var field_ids = cj('questions').attr('field-ids').split(',');
        window.field_labels = {};
        var requests = [];

        for (var i=0; i<field_ids.length; i++){
          requests.push(get_field_label(field_ids[i]));
        }
        var tpl = " <div class='crm-section form-item questions-item' ><div class='label'><label >[label]</label></div><div class='content'><textarea name='[field_name]' rows='2'></textarea></div><div class='clear'></div></div>";
        var button = "<div class='crm-submit-buttons submit-questions'><span class='crm-button crm-button-type-upload crm-i-button'><i class='crm-i fa-check'></i><input class='crm-form-submit default ' type='submit' value='Submit' style='width:2cm' crm-icon='fa-check'/></span></div>";

        cj.when.apply(cj, requests).done(function(schemas) {
          var full_text="";
          for(var i=0; i<field_ids.length; i++){
            var id  = field_ids[i];
            full_text+= tpl.replace('[label]', field_labels[id]).replace('[field_name]','custom_'+id);
          }
          full_text+= button;
          cj('questions').html(full_text);
          cj(document).on('click','.submit-questions input',function(e){
              e.preventDefault();
              var arr = {};
              cj('.questions-item textarea').each(function(){
                  var el = cj(this);
                  if (el.val() == '') return;
                  arr [ el.attr('name')] = el.val();
              });

              if (Object.keys(arr).length ==0 ) return;

              arr.sequential =1;
              arr.id = window.contrib_id;
              CRM.api3('Contribution', 'create', arr).done(function(){
                cj('questions').html('Thanks for feedback!');
              });
          });
        });


      });
      
</script>
{/literal}